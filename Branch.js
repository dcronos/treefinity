var Branch = new Phaser.Class({

    Extends: Phaser.GameObjects.Image,

    initialize:

    function Branch (scene)
    {
    	var branchNum = Math.floor(Math.random() * 2) + 1;
		var branchSpr = "branch" + branchNum;
        Phaser.GameObjects.Image.call(this, scene, 0, 0, branchSpr);

        this.setActive(true);
        this.setVisible(true);
    },

    setPosX: function (x, flipX)
    {
        this.x = x;
        this.flipX = flipX;	      
    },

    setPosY: function (y)
    {
        this.y = y;

        this.setActive(true);
        this.setVisible(true);
    },

    update: function (time, delta)
    {
    	//var mat = this.getWorldTransformMatrix();
		//var y = mat.getY(0, 0);
		//var y = this.y - mainCam.scrollY * this.scrollFactorY;
		//var scrollY = mainCam.y - mainCam.scrollY * mainCam.scrollFactorY;

		var mat = this.getWorldTransformMatrix();
		var cam = this.scene.cameras.main;
		// Get world position;
		var y = mat.getY(0, 0);
		var x = mat.getX(0, 0);

		// Convert world position into canvas pixel space
		var displayScale = cam.scaleManager.displayScale;
		mat = cam.matrix;
		let ty = mat.getY(x - cam.scrollX, y - cam.scrollY) / displayScale.y;
		y = Math.round(ty);

    	//console.log("y pos " + config.bottomLimit);
    	
        if (y > config.bottomLimit)
        {
            this.setActive(false);
            this.setVisible(false);
        }
    }
});