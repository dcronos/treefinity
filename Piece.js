class Piece extends Phaser.GameObjects.Container{
	constructor(scene, pos, pieceDirection, speed){

		var x = 75;
		//x = pieceDirection ? 10 : config.width - 200;
		var y = pos + 2;

		super(scene, x, y, null);
		scene.add.existing(this);

		this.rightLimit;
		this.right;
		this.left;
		this.moveDirection = pieceDirection;
		this.canMove = true;
		this.speed = speed;
		var chopSide = scene.chopSides.noChop;
	}

	update(){
		if(this.canMove){
			if (!this.moveDirection)
			{
				if (this.x < this.left){
					this.moveDirection = true;
					return;
				}

				this.x -= this.speed;
				//this.y += 3 * this.speed;

					//pos = Vector3.MoveTowards(pos, leftPos, speed * Time.deltaTime);
			}
			else if (this.moveDirection)
			{
				if (this.x > this.right){
					this.moveDirection = false;
					return;
				}
				this.x += this.speed;
				//this.y -= 3 * this.speed;
					//pos = Vector3.MoveTowards(pos, rightPos, speed * Time.deltaTime);
			}
		}
	}

	startPosition(bounds){
		//console.log("WIDTH " + bounds.width);
		this.right = config.width / 2 + this.rightLimit;
		this.left = config.width / 2 - bounds.width * 1.6;
		this.x = pieceDirection ? this.right : this.left;
		//console.log("piece x " + this.x);
	}

	stopMove(){
		this.canMove = false;
	}
}
