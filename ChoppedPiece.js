class ChoppedPiece extends Phaser.GameObjects.Container{
	constructor(scene, posX, posY){

		super(scene, posX, posY);
		scene.add.existing(this);

		//this.play("beam_anim");
		scene.physics.world.enableBody(this);
		//this.setOrigin(0.5, 0.5);
		this.chop = false;
		//this.body.velocity.y = -250;

		scene.currentPiece.add(this);
	}

	update(){
		if(this.chop){
			console.log("chop y " + this.y);
			if(this.y > 200){
				console.log("destroy slice");
				//this.removeAll();
				this.chop = false;
				//this.destroy(true);
				//this.removeAll();
			}	
		}
		/*
			console.log("slice y " + this.y);
			
		}

		/*if(this.y < 16){
			this.destroy();
		}*/
	}

	chopPiece(){
		this.chop = true;
		this.body.setVelocity(40, 50);
		this.body.setGravityY(400);
	}
}