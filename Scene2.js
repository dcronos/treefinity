var pieceHeight;
var mainCam;
var previousPiece;
//var chopedPiece;
var removedPieces = {};
var rightLimit;
var perfectCounter = 0;
var perfectCounterRepop = 1;
var pieceDirection = true;
var speed;
var fontSize = 100;
var pieceMatchDiff = 5;
var totalSlices = 50;
var trunkHolder;
var branchHolder;
var sliceWidth;
var gameOver;
var _UIScene;
var isPaused;

var debugText;

class Scene2 extends Phaser.Scene{

	constructor(){
		super("playGame");
	}

	create(){
		//console.log("creating game");
		speed = 7;
		isPaused = 0;
		gameOver = false;
		rightLimit = 250;
		trunkHolder = [];
		//branchHolder = [];
		this.gameStart = false;
		this.gameStarted = false;
		this.pieceCount = totalSlices;
		this.canTap = true;
		this.offset = 29;

		this.chopSides = {
			noChop : 0,
			leftChop : 1,
			rightChop : 2,
		}

		this.initParticleEmitters();

		//this.gradient = this.add.sprite(config.width, config.height, "gradient");
		//this.gradient.setScale(config.width, this.gradient.scaleY * 3);

		this.treesLogo = this.add.image(0, 100, "treesLogo");
		this.treesLogo.x = config.width / 2;		

		this.appName = this.add.image(config.width / 2, 0, "logo");
		this.appName.y = this.treesLogo.y + this.treesLogo.height + 50;

		this.base1 = this.add.image(config.width/2, config.height - 105, "base");
		
		this.base2 = this.add.image(config.width/2 - 4, this.base1.y - 170, "isoBase");
		pieceHeight = this.base2.height - this.offset;
		sliceWidth = this.base2.width / totalSlices;
		pieceMatchDiff = sliceWidth + 1;
		
		previousPiece = this.base2;	
		previousPiece.name = "first";

		this.piecePosition = this.base2.y - pieceHeight;

		this.tapToStart = this.add.text(0, 0, 'Click to start', { font: '60px Courier', fill: '#FFFFFF', stroke: '#000000', align: 'center' });
		this.tapToStart.x = config.width / 2 - this.tapToStart.width / 2;
		this.tapToStart.y = config.height / 2;
		
		this.studioLogo = this.add.image(0, 0, "studioLogo");
		this.studioLogo.x = this.studioLogo.width / 2 + 20;		
		this.studioLogo.y = config.height - this.studioLogo.height;

		this.copyright = this.add.text(0, 0, '© 2019 Pixpik Studio Production', { font: '20px Courier', fill: '#FFFFFF', stroke: '#000000', align: 'center' });
		this.copyright.x = config.width - this.copyright.width - 30;
		this.copyright.y = config.height - this.copyright.height * 2;

		this.chopFx = this.add.sprite(0, 0, "fx_cut");
		this.chopFx.setActive(false).setVisible(false).setDepth(1);
		this.chopFx.setScale(1.5,1.5);

		this.stackFxRight = this.add.sprite(0, 0, "fx_stack");
		this.stackFxRight.setActive(false).setVisible(false).setDepth(1);
		this.stackFxRight.setScale(1.5,1.5);

		this.stackFxLeft = this.add.sprite(0, 0, "fx_stack");
		this.stackFxLeft.flipX = true;
		this.stackFxLeft.setActive(false).setVisible(false).setDepth(1);
		this.stackFxLeft.setScale(1.5,1.5);

		this.startSFX = this.sound.add("startSFX");
		this.stackSFX = this.sound.add("stackSFX");
		this.stackGrow1SFX = this.sound.add("stackGrow1SFX");
		this.stackGrow2SFX = this.sound.add("stackGrow2SFX");
		this.perfectStackSFX = this.sound.add("perfectStackSFX");
		this.growBranchSFX = this.sound.add("growBranchSFX");
		this.gameOverSFX = this.sound.add("gameOverSFX");
		this.finalLeaves = this.sound.add("finalLeaves");
		this.backgroundBirds = this.sound.add("backgroundBirds");
		this.birdsConfig = {
			mute : false,
			volume : 1,
			rate : 1,
			detune: 0, 
			seek: 0, 
			loop: true,
			delay: 0
		}
		this.backgroundBirds.play(this.birdsConfig);

		game.input.mouse.capture = true;

		mainCam = this.cameras.main;
		game.scene.remove('uiscene');
		//this.UICam = this.cameras.add(0, 0, config.width, config.height).setBackgroundColor(0x528ccb);
		_UIScene = game.scene.add('uiscene', UIScene, true, { x: config.x, y: config.y });

		//debugText = this.add.text(10, 10, 'Move the mouse', { font: '16px Courier', fill: '#00ff00' });

		branchHolder = this.add.group({
	        classType: Branch,
        	maxSize: 15,
        	runChildUpdate: true
	    });

	    //this.info = this.add.text(300, 0, 'Click to add objects', { fill: '#00ff00' });
	    //this.info.setScrollFactor(0)

	}

	update(){

		var pointer = this.input.activePointer;
		//this.UICam.y = mainCam.y;
				/*if(gameOver && (game.input.mousePointer.isDown || pointer.isDown)){
			//this.input.once('pointerdown', function () {
			console.log("input restart");
			this.events.emit('restart');
			
		} */
		if(!this.gameStart && (game.input.mousePointer.isDown || pointer.isDown)){
			//this.input.once('pointerdown', function () {
           // this.scene.restart();
           		//console.log("start");
           		//this.startSFX.play();
           		this.asyncPlaySound(this.startSFX);
				this.gameStart = true;
				this.initGame();
        	//}, this);
		}
		else if(this.gameStarted && !gameOver){
			this.currentPiece.update();
			this.updateFallingSlices();

			if(isPaused > 0){
				//console.log("unpause");
				isPaused--;
				return;
			}
			else if(game.input.mousePointer.isDown || pointer.isDown){
				//console.log("tap slice");
				if(this.canTap){
					this.canTap = false;
					this.currentPiece.stopMove();

					var prevBounds = previousPiece.getBounds(prevBounds);
					//console.log("current x " + this.currentPiece.x);
					//console.log("prevBounds x " + prevBounds.x);
					if(previousPiece.name !== "first"){
						//console.log("chop side before" + previousPiece.chopSide);
						//console.log("chop side " + previousPiece.chopSide);
						//console.log("left " + this.chopSides.leftChop);
						//console.log("right " + this.chopSides.rightChop);
						switch(previousPiece.chopSide){
							case this.chopSides.leftChop:
							//console.log("bounds left");
							prevBounds = this.getPreviousPieceBounds();
							if(this.currentPiece.x > prevBounds.x)
								prevBounds = previousPiece.getBounds(prevBounds);
							break;
							//case this.chopSides.rightChop:
							//console.log("bounds right");
							//prevBounds = this.getPreviousPieceBounds();
							break;
						}
						//onsole.log("ajusted prevBounds x" + prevBounds.x);
					}

					var perfectMatch = this.checkPieceMatch(prevBounds);
					if(!perfectMatch){
						this.checkSliceCollision(prevBounds);
					}
					//this.createPerfectMatchBranch(0);
					this.addScore();
					//this.savePreviousSlices();
					this.nextPiece();
				}
			} else {
				this.canTap = true;
			}
		}

		/*debugText.setText([
	        'x: ' + pointer.x,
	        'y: ' + pointer.y
	        //'mid x: ' + pointer.midPoint.x,
	        //'mid y: ' + pointer.midPoint.y,
	        //'velocity x: ' + pointer.velocity.x,
	        //'velocity y: ' + pointer.velocity.y,
	        //'movementX: ' + pointer.movementX,
	        //'movementY: ' + pointer.movementY
    	]);*/

    	/*this.info.setText([
        	'Used: ' + branchHolder.getTotalUsed(),
        	'Free: ' + branchHolder.getTotalFree()
    	]);*/
	}

	initGame(){
		removedPieces = {};
		
		this.treesLogo.destroy();
		this.appName.destroy();
		this.tapToStart.destroy();
		this.studioLogo.destroy();
		this.copyright.destroy();

		this.currentPiece = new Piece(this, this.piecePosition, pieceDirection, speed);
		pieceDirection = !pieceDirection;
		this.choopedPiece;
		this.piecesToRemove = [];
		this.currentPiece.rightLimit = rightLimit;
		this.createSlices();
		this.pieceBounds = this.currentPiece.getBounds(this.pieceBounds);
		this.currentPiece.startPosition(this.pieceBounds);

		this.score = 0;

		_UIScene.addLogo();
		//mainCam.ignore([this.scoreLabel, this.treesLogo]);
		//this.UICam.ignore(this.base1, this.base2);

		var timedEvent = this.time.delayedCall(100, this.onEvent, [], this);
	}

	initParticleEmitters(){
		var particles = this.add.particles('particles');
    	var fig1 = particles.createEmitter({
		    frame: 0,
		    x: {min: 100, max: config.width - 100},
		    y: {min: 100, max: config.height - 100},
		    speed: { min: 50, max: 150 },
		    frequency: 4000,
		    lifespan: 12000
    	});
    	fig1.setScrollFactor(0);
    	var fig2 = particles.createEmitter({
		    frame: 1,
		    x: {min: 100, max: config.width - 100},
		    y: {min: 100, max: config.height - 100},
		    speed: { min: 25, max: 70 },
		    frequency: 3000,
		    lifespan: 12000
    	});
    	fig2.setScrollFactor(0);
    	particles.emitters.sendToBack(fig1);
    	particles.emitters.sendToBack(fig2);
	}

	asyncPlaySound(sound){
		setTimeout(function() {
			sound.play();
		}, 50);
	}

	onEvent ()
	{
		//console.log("game started");
		this.gameStarted = true;
	}

	nextPiece(){
		if(!gameOver){
			//console.log("piece creation");
			speed += 0.1;

			previousPiece = this.currentPiece;

			var position = this.currentPiece.y;
			var adjustedSpeed = speed;
			if(this.score > 35){
				var randomSpeed = Math.floor(Math.random() * 3);
				if(randomSpeed === 0)
					adjustedSpeed *= .7;
			} 
			this.currentPiece = new Piece(this, position - pieceHeight, pieceDirection, adjustedSpeed);
			pieceDirection = !pieceDirection;
			this.currentPiece.rightLimit = rightLimit;
			this.createSlices();
			this.currentPiece.startPosition(this.pieceBounds);
			if(this.score > 3){
				//console.log("pan");
				setTimeout(this.panCamera(), 50);
				
				//mainCam.centerOnY(currentPiece.y);
				//this.UICam.centerOnY(currentPiece.y);
			}
		}
	}

	panCamera(){
		mainCam.pan(config.width / 2, this.currentPiece.y, 400);
		//this.UICam.pan(config.width / 2, this.currentPiece.y, 400);
	}

	checkSliceCollision(prevBounds){
		var numRemovedPieces = 0;
		var bounds = this.currentPiece.getBounds(bounds);
		//var prevBounds = previousPiece.getBounds(prevBounds);
		//this.getPreviousPieceBounds();
		//console.log("current x " + bounds.x);
		//console.log("current bounds " + bounds.width);
		//console.log("prev x " + prevBounds.x);
		//console.log("prev bounds " + prevBounds.width);
		var leftChop = bounds.x < prevBounds.x ? false : true;
		this.currentPiece.chopSide = leftChop ? this.chopSides.rightChop : this.chopSides.leftChop;
		//console.log("chop side " + this.currentPiece.chopSide);
		//if(leftChop){
			//prevBounds = this.getPreviousPieceBounds();
			//console.log("new bounds " + prevBounds.x);
			/*if(leftChop){
				for(var i = this.currentPiece.list.length - 1; i == 0; i++){
					var curr = this.currentPiece.getAt(i);
					var currBounds = curr.getBounds(currBounds);
					var result = Phaser.Geom.Intersects.RectangleToRectangle(currBounds, prevBounds);
					console.log("slice couter " + i);
					if(numRemovedPieces > 0 && result)
						break;
					if(!result){
						numRemovedPieces++;
						this.pieceCount--;
						rightLimit += sliceWidth;
						removedPieces[curr.name] = true;
						curr.chopSlice(!leftChop);
						this.piecesToRemove.push(curr);
					} 
				}
			} else {*/
				for(var i = 0; i < this.currentPiece.list.length; i++){
					var curr = this.currentPiece.getAt(i);
					var currBounds = curr.getBounds(currBounds);
					//console.log("prev bounds " + prevBounds.x);
					//console.log("curr " + bounds.x);
					//console.log("prevBounds " + prevBounds.x);
					//leftChop = 
					var result = Phaser.Geom.Intersects.RectangleToRectangle(currBounds, prevBounds);
					//console.log("slice couter " + i);
					if(numRemovedPieces > 0 && result)
						break;
					if(!result){
						numRemovedPieces++;
						this.pieceCount--;
						rightLimit += sliceWidth;
						removedPieces[curr.name] = true;
						curr.chopSlice(!leftChop);
						this.piecesToRemove.push(curr);
					} 
				}
			//}
		//}
		

		this.chopFX(numRemovedPieces, bounds, leftChop);
		//this.currentPiece.size.setWidth(widthMinus);
		//for(var i = 0; i < this.piecesToRemove.length; i++){
			//this.currentPiece.remove(this.piecesToRemove[i]);
		//}

		if(this.pieceCount === 0){
			//this.gameOverSFX.play();
			gameOver = true;
			var zoomOut;
			if(this.score < 8){
				zoomOut = 1;
			}
			else{
				zoomOut = 0.6;
			}
			mainCam.zoomTo(zoomOut, 1000);
			//fontSize = 350;
			//this.scoreLabel.setFontSize(fontSize);

			this.finalLeaves.play();
			this.foliage = this.add.sprite(prevBounds.x + prevBounds.width / 2, previousPiece.y - 50, "fx_foliage");
			this.foliage.setScale(1.2,1.2);
			this.foliage.play("fxFoliage");

			var timedEvent = this.time.delayedCall(1100, this.gameOverEvent, [], this);
			//console.log("game over");
		} else {
			var createBranch = Math.floor(Math.random() * 2);
			if(createBranch === 0){
				var branchSide = leftChop ? 2 : 1;
				//this.createPerfectMatchBranch(branchSide);
			}
		}
	}

	chopSliceWhenCollideAtIndex(i){
		
	}

	getPreviousPieceBounds(){
		var bounds = previousPiece.getBounds(bounds);
		if(previousPiece.name !== "first"){
			var len = previousPiece.list.length;
			var lenWidth = len * sliceWidth;
			if(bounds.width > lenWidth){
				//bounds.width = lenWidth;
				var xDelta = bounds.width - lenWidth;
				bounds.x = bounds.x + xDelta;
			}
		}
		return bounds;
	}

	gameOverEvent ()
	{
		_UIScene.addGameOverButtons();
	}

	chopFX(numRemovedPieces, bounds, leftChop){
		this.asyncPlaySound(this.stackSFX);
		if(numRemovedPieces > 0){
			this.chopFx.setActive(true).setVisible(true);
			this.chopFx.y = bounds.y + bounds.height / 2;
			if(!leftChop){//right chop
				this.chopFx.x = bounds.x + (numRemovedPieces * sliceWidth);
			}else {//left chop
				this.chopFx.x = bounds.x + bounds.width - (numRemovedPieces * sliceWidth);
			}
			this.chopFx.play("fxCut");
		}
	}

	checkPieceMatch(prevBounds){
		var repop = 0;
		var currentBounds = this.currentPiece.getBounds(currentBounds);
		//console.log("previos x " + prevBounds.x);
		//console.log("current bounds x " + currentBounds.x);
		//console.log("current x " + this.currentPiece.x);
		//console.log("current bounds width " + currentBounds.width);
		//console.log("current width " + this.currentPiece.width);
		//console.log("previos width" + prevBounds.width);
		//console.log("current width" + currentBounds.width);
		var diff = Math.abs(prevBounds.x - currentBounds.x)
		//console.log("diff " + diff);
		if(diff <= pieceMatchDiff){
			this.asyncPlaySound(this.perfectStackSFX);
			perfectCounter++;
			if(previousPiece.name === "first"){
				var offset = 168;
				this.currentPiece.x = previousPiece.x - offset;
			} else {

				var pieceCounter = this.currentPiece.list.length;
				//console.log("piece counter " + pieceCounter);
				if(pieceCounter < totalSlices && perfectCounter >= perfectCounterRepop){
					rightLimit -= sliceWidth;
					var piece = this.currentPiece.getAt(0);
					if(piece.name !== 0){
						repop = 1;
						var index = parseInt(piece.name, 10);
						index--;
						this.createSliceWithIndex(index, piece.x, -sliceWidth, true);
					} else {
						repop = 2;
						//console.log("piece " + piece);
						//console.log("length " + this.currentPiece.length);
						//console.log("length - 1" + (pieceCounter - 1));
						piece = this.currentPiece.getAt((pieceCounter - 1));
						var index = parseInt(piece.name, 10);
						index++;
						this.createSliceWithIndex(index, piece.x, sliceWidth, false);
					}
				}

			}
			//this.currentPiece.x = prevBounds.x;
			//if(this.pieceCount === 50 || repop === 1){
				//console.log("50 slices");
				//this.currentPiece.x = prevBounds.x + sliceWidth/2;
			//}
			//else if(repop === 2){
				this.currentPiece.x = prevBounds.x + sliceWidth / 2;
			//}
			this.createPerfectMatchBranch(repop);
			this.matchStackFX(currentBounds);
			return true;
		} else 
			perfectCounter = 0;

			return false;
	}

	createPerfectMatchBranch(repop){
//		this.growBranchSFX.play();
		this.asyncPlaySound(this.growBranchSFX);
		var bounds = this.currentPiece.getBounds(bounds);

		var branch = branchHolder.get();//this.add.image(0, 0, branchSpr);
		//branch.y = ;
		branch.setPosY(bounds.y + bounds.height / 2);
		branch.scaleX = 0.1;
		branch.scaleY = 0.1;
		switch(repop){
			case 0:
			//branch.flipX = true;
			//branch.x =  bounds.x - branch.width / 2;
			branch.setPosX(bounds.x - branch.width / 2, true);
			var branch2 = branchHolder.get();//this.add.image(0, 0, branchSpr);
			//branch2.y = branch.y;
			branch2.setPosY(branch.y);
			branch2.scaleX = 0.1;
			branch2.scaleY = 0.1;
			//branch2.x = bounds.x + bounds.width + branch.width / 2;
			branch2.setPosX(bounds.x + bounds.width + branch.width / 2, false);
			this.tweens.add({
            targets: branch2,
            scaleX: 1,
            scaleY: 1,
            ease: 'Sine.easeInOut',
            duration: 300,
            repeat: 0,
        });
			break;
			case 1:
			//branch.x = bounds.x + bounds.width + branch.width / 2;
			branch.setPosX(bounds.x + bounds.width + branch.width / 2);
			break;
			case 2:
			//branch.flipX = true;
			branch.setPosX(bounds.x - branch.width / 2, true);
			//branch.x =  bounds.x - branch.width / 2;
			break;
		}

		//branchHolder.push(branch);
		//branchHolder.push(branch2);

		this.tweens.add({
            targets: branch,
            scaleX: 1,
            scaleY: 1,
            ease: 'Sine.easeInOut',
            duration: 300,
            repeat: 0,
        });
	}

	matchStackFX(bounds){
		this.stackFxRight.setActive(true).setVisible(true);
		this.stackFxRight.y = bounds.y + bounds.height * .6;
		this.stackFxRight.x =  bounds.x + bounds.width + this.stackFxRight.width / 3;
		this.stackFxRight.play("fxStack");

		this.stackFxLeft.setActive(true).setVisible(true);
		this.stackFxLeft.y = bounds.y + bounds.height * .6;
		this.stackFxLeft.x =  bounds.x - this.stackFxRight.width / 3;
		this.stackFxLeft.play("fxStack");

		this.add.tween({
		  targets: [this.stackFxRight, this.stackFxLeft],
		  ease: 'Sine.easeInOut',
		  duration: 700,
		  delay: 0,
		  alpha: {
		    getStart: () => 1,
		    getEnd: () => 0
		  }
		});
	}
	
	createSliceWithIndex(index, xPos, xOffset, adFirst){
		this.pieceCount++;
		var part = "slice" + index;
		//console.log("create slice " + part);
		var slice = new Slice(this, xPos + xOffset, 0, part);
		slice.name = index;
		removedPieces[index] = false;
		this.currentPiece.add(slice);
	}

	createSlices(){
		this.currentPiece.removeAll();
		var posX = 0;
		var posY = 0;
		for(var i = 0; i < totalSlices; ++i){
			var key = i;
			if(removedPieces[key]){
				continue;
			}
			var slicePart = "slice" + i;
			var slice = new Slice(this, posX, posY, slicePart);
			posX += sliceWidth;
			slice.name = i;
			this.currentPiece.add(slice);
		}
		trunkHolder.push(this.currentPiece);
			if(trunkHolder.length > 13){
				trunkHolder[0].removeAll();
				trunkHolder[0].destroy();
				trunkHolder.shift();
				/*if(branchHolder > 6){
					branchHolder.shift();
					branchHolder.shift();
				}*/
			}
	}

	savePreviousSlices(){
		if(previousPiece.name !== "first"){
			previousPiece.removeAll();
		
			for(var i = 0; i < this.currentPiece.list.length; i++){
				var slice = this.currentPiece.getAt(i);
				previousPiece.add(Object.assign({}, slice));
			}
		}
	}

	updateFallingSlices(){
		if(this.piecesToRemove.length > 0){
			for(var i = 0; i < this.piecesToRemove.length; i++){
				var slice = this.piecesToRemove[i];
				slice.update(this.piecesToRemove);
			}
		}
	}

	addScore(){
		this.score += 1;
		_UIScene.updateScore(this.score);
	}

	setHud(){
		var graphics = this.add.graphics();
	    graphics.fillStyle(0x000000, 1);
	    graphics.beginPath();
	    graphics.moveTo(0, 0);
	    graphics.lineTo(config.width, 0);
	    graphics.lineTo(config.width, 20);
	    graphics.lineTo(0, 20);
	    graphics.lineTo(0, 0);
	    //
	    graphics.closePath();
	    graphics.fillPath();
	    graphics.setScrollFactor(0);
	}
}

class UIScene extends Phaser.Scene {

    constructor (config)
    {
        super(config);
    }

    preload ()
    {
        //this.load.image('face', 'assets/pics/bw-face.png');
    }

    create ()
    {	
    	this.isPaused = false;
    	//this.add.dynamicBitmapText(200, config.height * .1, 'droid', '100', 100);
    	this.scoreLabel = this.add.text(0, config.height * .15, '', { font: '100px Arial', fill: '#FFFFFF' });
		this.scoreLabel.x = config.width / 2 - this.scoreLabel.width / 2;	

		this.bestScoreLabel = this.add.text(0, this.scoreLabel.y + 150, '', { font: '70px Arial', fill: '#FFFFFF' });
		this.bestScoreLabel.setVisible(false);
    }

    addLogo(){
    	this.btn_pause = this.add.image(60, 60, "btn_pause").setInteractive();
		this.btn_pause.on('pointerdown', this.pausePressed, this);

    	this.treesLogo = this.add.image(0, 100, "treesLogo");
		this.treesLogo.x = config.width - this.treesLogo.width / 2;
		this.treesLogo.setScale(0.6, 0.6);
    }

    addGameOverButtons(){

    	this.bestScoreLabel.setVisible(true);
    	this.bestScoreLabel.text = this.scoreLabel.text;
    	var best = localStorage.getItem('bestScore');
    	if(!best){
    		localStorage.setItem('bestScore', this.scoreLabel.text);
    	} else {
    		var bestNum = parseInt(this.scoreLabel.text, 10);
    		var bestSaved = parseInt(best, 10);
    		if(bestNum > bestSaved){
    			localStorage.setItem('bestScore', bestNum);
    		}
    		else {
    			this.bestScoreLabel.text = bestSaved;
    		}
    	}
    	this.bestScoreLabel.x = config.width / 2 - this.bestScoreLabel.width / 2;	

    	this.btn_pause.setActive(false).setVisible(false);

    	this.treesLogo.x = config.width / 2;
    	this.treesLogo.setScale(1, 1);

    	this.tryAgainBtn = this.add.image(0, 0, "tryAgainBtn").setInteractive();
		this.tryAgainBtn.x = config.width / 2;
		this.tryAgainBtn.y = config.height / 2 + this.tryAgainBtn.height;	

    	this.learnMoreBtn = this.add.image(0, 0, "learnMoreBtn").setInteractive();
		this.learnMoreBtn.x = config.width / 2;
		this.learnMoreBtn.y = this.tryAgainBtn.y + this.tryAgainBtn.height * 1.5;	

		this.learnMoreBtn.on('pointerup', this.learnMorePressed, this);	

		this.tryAgainBtn.on('pointerup', this.restartEvent, this);
    }

    updateScore(score){
    	this.scoreLabel.text = score;
		this.scoreLabel.x = config.width / 2 - this.scoreLabel.width / 2;
    }

    restartEvent(){
		this.scene.start("bootGame");
	}

	pausePressed(){
		if(isPaused === 0){
			isPaused = 10;
			game.scene.pause('playGame');
		}else {
			game.scene.resume('playGame');
		}
	}

	learnMorePressed(pointer, gameObject){
		var url = 'https://trees.org';

	    var s = window.open(url, '_blank');

	    if (s && s.focus)
	    {
	        s.focus();
	    }
	    else if (!s)
	    {
	        window.location.href = url;
	    }
	}
}
