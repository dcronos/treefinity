class Slice extends Phaser.GameObjects.Image{
	constructor(scene, posX, posY, sprite){

		super(scene, posX, posY, sprite);
		scene.add.image(this);

		scene.physics.world.enableBody(this);
		this.setOrigin(0.5, 0.5);
		this.chop = false;
		this.destroyPoint = 0;
		//scene.currentPiece.add(this);
	}

	update(){
		if(this.chop){
			if(this.y > this.destroyPoint){
				//console.log("destroy slice");
				this.chop = false;
				this.destroy();
			}
		}
	}

	chopSlice(leftChop){
		this.chop = true;
		this.destroyPoint = this.y + 300;
		var xForce = leftChop ? -75 : 75;
		this.body.setVelocity(xForce, 130);
		this.body.setGravityY(400);
	}
}