
var configMobile = {
	width: window.innerWidth,
	height: window.innerHeight,
	backgroundColor: 0x528ccb,
	scene: [Scene1, Scene2],
	audio: {
        disableWebAudio: true
    },
	pageAlignHorizontally: true,
	pageAlignVertically: true,
	physics: {
		default: 'arcade',
		arcade: {
			//gravity: { y: 200 },
			debug: false
		}
	},
	/*fps: {
		target: 5,
		min: 5,
		forceSetTimeOut: true
	}*/
	bottomLimit: 1800
}

var configDesktop = {
	width: 800,
	height: 1300,
	backgroundColor: 0x528ccb,
	scene: [Scene1, Scene2],
	scale: {
        mode: Phaser.Scale.HEIGHT_CONTROLS_WIDTH,
        autoCenter: Phaser.Scale.CENTER_BOTH
    },
	physics: {
		default: 'arcade',
		arcade: {
			//gravity: { y: 200 },
			debug: false
		}
	},
	/*fps: {
		target: 30,
		min: 30,
		forceSetTimeOut: true
	}*/
	bottomLimit: 1000
}

var config = {}

var gameSettings = {}

if(window.innerWidth > window.innerHeight){
	config = configDesktop;
}
else{
	config = configMobile;
}
var game = new Phaser.Game(config);
//}