class Scene1 extends Phaser.Scene{
	constructor(){
		super("bootGame");
		/*game.scale.pageAlignHorizontally = true;
		game.scale.pageAlignVertically = true;
		game.scale.refresh();*/
	}

	preload(){
		//this.load.image("gradient", "assets/images/gradBackground.png");
		this.load.image("btn_pause", "assets/images/pause.png");
		this.load.image("learnMoreBtn", "assets/images/btn_info.png");
		this.load.image("tryAgainBtn", "assets/images/btn_replay.png");
		this.load.image("studioLogo", "assets/images/studioLogo.png");
		this.load.image("treesLogo", "assets/images/treesLogo.png");
		//this.load.image("isoSlice", "assets/images/isoSlice.png");
		this.load.image("isoBase", "assets/images/isoBaseNew2.png");
		this.load.image("base", "assets/images/spr_base.png");
		this.load.image("logo", "assets/images/spr_logo.png");
		this.load.image("branch1", "assets/images/spr_rama_01.png");
		this.load.image("branch2", "assets/images/spr_rama_02.png");
		
		/*for(var i = 0; i < 10; i++){
			var name = "slice" + i;
			var n = i;
			var fileName = "slice" + (n + 1) + ".png";
			var path = "assets/images/" + fileName;
			this.load.image(name, path);
		}*/

		this.load.spritesheet("fx_cut", "assets/spritesheets/FX_Cut2.png",{
			frameWidth: 57,//114,
			frameHeight: 67//134
		});

		this.load.spritesheet("fx_stack", "assets/spritesheets/FX_StackClear.png",{
			frameWidth: 67,//134,
			frameHeight: 89//178
		});

		this.load.spritesheet("fx_foliage", "assets/spritesheets/FX_Foliage.png",{
			frameWidth: 494,
			frameHeight: 376
		});

		this.load.spritesheet('particles', 'assets/spritesheets/particles.png', { frameWidth: 70, frameHeight: 70 });

		for(var i = 0; i < 50; i++){
			var name = "slice" + i;
			var n = i;
			var fileName = "slice" + (n + 1) + ".png";
			var path = "assets/images/slices/" + fileName;
			this.load.image(name, path);
		}

		//this.load.bitmapFont("pixelFont", "assets/font/font.png", "assets/font/font.xml");
		this.load.bitmapFont('droid', 'assets/font/droid.png', 'assets/font/droid.fnt');

		this.load.audio("backgroundBirds", ["assets/sounds/Background Birds.mp3"]);
		this.load.audio("startSFX", ["assets/sounds/start.mp3"]);
		this.load.audio("stackSFX", ["assets/sounds/stack.mp3"]);
		this.load.audio("stackGrow1SFX", ["assets/sounds/stackGrow1.mp3"]);
		this.load.audio("stackGrow2SFX", ["assets/sounds/stackGrow2.mp3"]);
		this.load.audio("perfectStackSFX", ["assets/sounds/perfectStack.mp3"]);
		this.load.audio("growBranchSFX", ["assets/sounds/growBranch.mp3"]);
		this.load.audio("gameOverSFX", ["assets/sounds/gameOver.mp3"]);
		this.load.audio("finalLeaves", ["assets/sounds/finalLeaves.mp3"]);

		/*this.load.audio("backgroundBirds", ["assets/sounds/Background Birds.mp3", "assets/sounds/Background Birds.m4a"]);
		this.load.audio("startSFX", ["assets/sounds/start.mp3", "assets/sounds/start.m4a"]);
		this.load.audio("stackSFX", ["assets/sounds/stack.mp3","assets/sounds/stack.m4a"]);
		this.load.audio("stackGrow1SFX", ["assets/sounds/stackGrow1.mp3", "assets/sounds/stackGrow1.m4a"]);
		this.load.audio("stackGrow2SFX", ["assets/sounds/stackGrow2.mp3", "assets/sounds/stackGrow2.m4a"]);
		this.load.audio("perfectStackSFX", ["assets/sounds/perfectStack.mp3", "assets/sounds/perfectStack.m4a"]);
		this.load.audio("growBranchSFX", ["assets/sounds/growBranch.mp3", "assets/sounds/growBranch.m4a"]);
		this.load.audio("gameOverSFX", ["assets/sounds/gameOver.mp3", "assets/sounds/gameOver.m4a"]);
		this.load.audio("finalLeaves", ["assets/sounds/finalLeaves.mp3", "assets/sounds/finalLeaves.m4a"]);*/
	}

	create(){
		this.anims.create({
			key: "fxCut",
			frames: this.anims.generateFrameNumbers("fx_cut"),
			frameRate: 15,
			repeat: 0,
			hideOnComplete: true
		});

		this.anims.create({
			key: "fxStack",
			frames: this.anims.generateFrameNumbers("fx_stack"),
			frameRate: 8,
			repeat: 0,
			hideOnComplete: true
		});

		this.anims.create({
			key: "fxFoliage",
			frames: this.anims.generateFrameNumbers("fx_foliage", {
				start: 0,
				end: 10
			}),
			frameRate: 8,
			repeat: 0,
		});

		this.scene.start("playGame");
	}
}